from prometheus_client import start_http_server, Summary, Gauge
import serial
import random
import time

# Create a metric to track time spent and requests made.
REQUEST_TIME = Summary('request_processing_seconds', 'Time spent processing request')

ser = serial.Serial("/dev/ttyACM0", 9600)
g = Gauge('temp_of_room', 'The Temperature of the room')

def get_temp():
  cc=str(ser.readline())
  g.set(float(cc[2:][:-5]))

if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8008)
    # Generate some requests.
    while True:
      get_temp()
