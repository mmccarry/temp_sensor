const int sensor=A5; // Assigning analog pin A5 to variable 'sensor'
float vout; //temporary variable to hold sensor reading

void setup() {
    pinMode(sensor,INPUT); // Configuring sensor pin as input
    Serial.begin(9600);
    
}

void loop() {
    vout=analogRead(sensor); //Reading the value from sensor
    vout=(vout*500)/1024; // apply formula for the LM35DZ
    Serial.println(vout); // Print out the temp
    delay(1000); //Delay of 1 second for ease of viewing 
}
