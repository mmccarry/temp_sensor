# temp_sensor

##Simple temp sensor and server.

This is a simple application which connects an LM35 Temperature sensor to an arduino.

When this code is uploaded to the arduono and the device is hooked up to a computer then the server component can be run.
This is a Python3 application which reads the serial port that the arduio is connected to and then exports that temperated in a format which can be picked up by a prometheus server.

Temperature sensor is a LM35DZ connected to an Arduino Uno.
### LM35 
* Pin 1 -> GND
* Pin 2 -> A5
* Pin 3 -> 5V

### Notes
You will need to change the serial port to the exact port for your device in the server file.

## Installing
### Python
Create a virtual env for the python package:

`python3 -m venv .env`

Use the virtual env:

`source .env/bin/activate`

Install the packages:

`pip3 install -r requirements.txt`

Run the server:

`python3 temp_sensor_server.py`

The metrics for the temperature will be exposed on localhost:8000

### Arduino
Install the ardunio IDE from https://www.arduino.cc/en/main/software

Install the sketch to the device.

## TODO

This project is WIP.  SystemD scripts for the server are to be written along with error checking for the get line on the serial for the arduino.

A docker for prometheus and granfana might be added.